var socket;

var pengelolaHalaman = function(){
  return {
    rootScope: '',
    locationScope: '',
    stackHalaman: [{
      kedalaman: 0,
      alamat: 'home'
    }],
    paramPerpindahan: {},
    judulHalamanTerakhir: '',
    cariKedalaman: function(alamat){
      if(alamat == 'home')
        return 0;

      if(alamat == 'login')
        return 1;

      if(alamat == 'register2')
        return 1;

      if(alamat == 'timeline')
        return 2;

      if(alamat == 'my_profile')
        return 3;

      if(alamat == 'profile')
        return 3;

      if(alamat == 'status')
        return 4;

      if(alamat == 'notification')
        return 4;

      if(alamat == 'chicchat')
        return 4;

      if(alamat == 'shoppink')
        return 4;

      if(alamat == 'forum')
        return 4;

      if(alamat == 'make_a_post')
        return 4;

      if(alamat == 'private_chat')
        return 5;

      if(alamat == 'group_chat')
        return 5;
    },
    aturJudul: function(alamat){
      // this.rootScope.jumlah = '';
      if(alamat == 'home'){
        this.rootScope.class_header = 'header-hide';
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.judul = 'HOME';
      }
      else
      if(alamat == 'login'){
        this.rootScope.class_header = 'header-hide';
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.judul = 'LOGIN';
      }
      else
      if(alamat == 'register2'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'REGISTER';
      }
      else
      if(alamat == 'timeline'){
        this.rootScope.judul = 'HOME';
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = 'header-timeline';
      }
      else
      if(alamat == 'status'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'STATUS';
      }
      else
      if(alamat == 'my_profile'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'PROFILE';
      }
      else
      if(alamat == 'profile'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = this.judulHalamanTerakhir;
      }
      else
      if(alamat == 'notification'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'NOTIFICATION';
      }
      else
      if(alamat == 'chicchat'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'CHIC CHAT';
      }
      else
      if(alamat == 'shoppink'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'SHOPPINK';
      }
      else
      if(alamat == 'forum'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'FORUM';
      }
      else
      if(alamat == 'delivery'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'DELIVERY';
      }
      else
      if(alamat == 'private_chat'){
        this.rootScope.class_header_chat = '';
        this.rootScope.class_header = 'header-hide';
        this.rootScope.judul = 'CHAT';
      }
      else
      if(alamat == 'group_chat'){
        this.rootScope.class_header_chat = '';
        this.rootScope.class_header = 'header-hide';
        this.rootScope.judul = 'GROUP CHAT';
      }
      else
      if(alamat == 'make_a_post'){
        this.rootScope.class_header_chat = 'header-hide';
        this.rootScope.class_header = '';
        this.rootScope.judul = 'NEW POST';
      }
      else{
        if(alamat.name != undefined){
          var judul = alamat.name.length > 12
                      ? alamat.name.substr(0, 12) + '...'
                      : alamat.name;
          this.rootScope.judul = judul;
          this.judulHalamanTerakhir = judul;
        }else{
          this.rootScope.judul = '';
        }

        if(alamat.jumlah != undefined){
          this.rootScope.jumlah = '(' + alamat.jumlah + ')';
        }else{
          this.rootScope.jumlah = '';
        }
      }
    },
    halamanSaatIni: function(){
      return this.stackHalaman[this.stackHalaman.length - 1];
    },
    mundur: function(){
      console.log(this.stackHalaman);

      if(this.halamanSaatIni().alamat == 'home' || this.halamanSaatIni().alamat == 'timeline'){
        navigator.app.exitApp();
      }else{

        // Menghapus stack halaman saat ini
        this.stackHalaman.pop();
        console.log('pop', this.stackHalaman);

        // Pindah halaman
        this.locationScope.path(this.halamanSaatIni().alamat);

        // Mengatur judul halaman
        this.aturJudul(this.halamanSaatIni().alamat);

        // console.log('********');
        // console.log('********');
        // console.log('********');
        // console.log(this.rootScope);
        // console.log('********');
        // console.log('********');
        // console.log('********');
        // this.rootScope.$apply();
      }
    },
    ke: function(location, alamat, args){
      if(alamat != this.halamanSaatIni().alamat){

        // Pop halaman hingga terkecil*
        while((this.cariKedalaman(alamat) <= this.halamanSaatIni().kedalaman) && this.stackHalaman.length >= 0)
          this.stackHalaman.pop();

        // Halaman dimasukkan ke dalam stack
        this.stackHalaman.push({
          kedalaman: this.cariKedalaman(alamat),
          alamat: alamat
        });

        // Pindah halaman
        location.path(alamat, args);

        // Mengatur judul halaman
        this.aturJudul(this.halamanSaatIni().alamat);

        // Scroll ke atas
        window.scrollTo(0, 0);

        if(args != undefined){
          if(args.requestJudulHalaman != undefined && args.requestJumlah != undefined){
            this.aturJudul({name: args.requestJudulHalaman, jumlah: args.requestJumlah});
            return;
          }
          if(args.requestJudulHalaman != undefined){
            this.aturJudul({name: args.requestJudulHalaman, jumlah: undefined});
            return;
          }
        }
      }
    }
  }
}();

function onLoad(){
  document.addEventListener("deviceready", onDeviceReady, false);
}

function onDeviceReady(){
  console.log("jengDixi App siap digunakan");
  socket = io.connect('http://rcserv-lifeu.rhcloud.com');
  // socket = io.connect('http://localhost:8080');

  socket.on('register1', function(retVal){

    console.log('%%%');
    console.log('%%%');
    console.log('%%%');
    console.log('--> ' + retVal);
    console.log('%%%');
    console.log('%%%');
    console.log('%%%');

    if(retVal == 1){
      console.log('masuk---');
      scopePtr['home'].pindahKeVerifikasi();
      scopePtr['home'].$apply();
      return;
    }

    if(retVal == 2){
      alert('Error: email sudah digunakan');
      return;
    }

    if(retVal == 3){
      alert('Error: nomor hp sudah digunakan');
      return;
    }
  })

  socket.on('register2', function(username){
    GLOBAL_username = username;
    scopePtr['register2'].pindahKeTimeline();
    scopePtr['register2'].$apply();
  })

  socket.on('ambilNamaGambar', function(data){

    var lenDataPesanScope = scopePtr['chicchat'].dataPesan.length;
    
    // Pencocokan username yang tepat pada list chat, lalu memberi nama dan gambar pada
    // anggota list tersebut (satu anggota list)
    for(var j = 0; j < lenDataPesanScope; j++){
      if(scopePtr['chicchat'].dataPesan[j].usernamePenerima == data.username){
        // console.log(scopePtr['chicchat'].dataPesan[j]);
        scopePtr['chicchat'].dataPesan[j].gambar = data.gambar;
        scopePtr['chicchat'].dataPesan[j].nama = data.nama;
      }
      break;
    }

    // Selain mengubah pada bagian list chat, gambar dan nama pada variabel 
    // GLOBAL_ALL['pesan'] yang sesuai dengan username tsb juga ikut diubah
    // Bagian ini terlihat brute force :D
    var len = GLOBAL_ALL.pesan.length;
    for(var i = 0; i < len; i++){
      var usernameCheck = GLOBAL_ALL.pesan[i].usernamePenerima == GLOBAL_username
                          ? GLOBAL_ALL.pesan[i].usernamePengirim
                          : GLOBAL_ALL.pesan[i].usernamePenerima;

      if(data.username == usernameCheck && typeof GLOBAL_ALL.pesan[i].in_nama == "undefined"){
        GLOBAL_ALL.pesan[i].in_nama = data.nama;
        GLOBAL_ALL.pesan[i].in_gambar = data.gambar;
      }
    }

    // Apply halaman agar gambar dan nama berubah
    scopePtr['chicchat'].$apply();
  })

  socket.on('kirimPrivate', function(retVal){
    console.log(retVal);

    if(pengelolaHalaman.halamanSaatIni().alamat != 'private_chat')
      return;

    var len = scopePtr['private_chat'].dataPesan.length;
    var temp = len - 1;
    var leastSending = len;
    while(scopePtr['private_chat'].dataPesan[temp].tanggal == -1 && temp > -1){
      var leastSending = temp;
      temp--;
    }

    if(leastSending < len){
      // TO DO: Ganti angka ajaib 99
      scopePtr['private_chat'].dataPesan[leastSending].idPesan = retVal._id;
      scopePtr['private_chat'].dataPesan[leastSending].tanggal = retVal.tanggal;
    }

    scopePtr['private_chat'].$apply();
  });


  socket.on('pesanDibaca', function(idpsn){
    console.log('PESAN DIBACA - ' + idpsn);
    var len = GLOBAL_ALL.pesan.length;
    for(var i = len - 1; i > -1; i--){
      if(GLOBAL_ALL.pesan[i].idPesan == idpsn){
        GLOBAL_ALL.pesan[i].read = true;
        break;
      }
    }
    if(pengelolaHalaman.halamanSaatIni().alamat == 'private_chat'){
      scopePtr['private_chat'].$apply();
    }
    if(pengelolaHalaman.halamanSaatIni().alamat == 'chicchat'){
      // Melakukan update pada halaman chichhat, menunjukkan ada pesan baru
      scopePtr['chicchat'].updatePesan();
      scopePtr['chicchat'].$apply();
    }
  }); 

  socket.on('terimaPesan', function(pesan){
    console.log('TERIMA PESAN');
    // Terdapat sedikit keambiguan disini :/
    // pengelolaHalaman.halamanSaatIni().alamat yang diharapkan adalah 'private_chat'

    // Pesan masuk disimpan ke variabel GLOBAL_ALL['pesan']
    GLOBAL_ALL.pesan.unshift(pesan);

    // Melakukan update pada halaman chichhat, menunjukkan ada pesan baru
    scopePtr['chicchat'].updatePesan();

    if(pengelolaHalaman.halamanSaatIni().alamat == 'private_chat'){
      // Pesan masuk di push (maksudnya) ke halaman percakapan private_chat
      scopePtr['private_chat'].dataPesan.push(pesan);
    }

    // Melakukan apply pada halaman saat ini, agar pesan yang masuk muncul pada halaman saat ini
    // Note: baca diatas *halaman saat ini
    scopePtr[pengelolaHalaman.halamanSaatIni().alamat].$apply();


    if(pengelolaHalaman.halamanSaatIni().alamat == 'private_chat'){
      // menandai pesan sudah dibaca
      socket.emit('pesanDibaca', {
        idPesan: pesan.idPesan,
        usernamePengirim: pesan.usernamePengirim
      });
      scopePtr['private_chat'].gotoBottom();
    }
  })

  socket.on('pesanKembali', function(pesan){
  })

  socket.on('initAwal', function(dataInitAwal){
    
    // console.log(dataInitAwal);

    console.log('================');
    console.log('================');
    console.log(dataInitAwal);
    console.log('================');
    console.log('================');

    // Trigger fungsi
    scopePtr[pengelolaHalaman.halamanSaatIni().alamat].olahDataInitAwal(dataInitAwal);
    // scopePtr[pengelolaHalaman.halamanSaatIni().alamat].$apply();
  })

  socket.on('terimaNotifikasi', function(data){
    // console.log(data);
  })

  socket.on('bacaNotifikasi', function(idN){
    var len = GLOBAL_ALL.notifikasi.length;

    // Menandai notifikasi telah dibaca
    for(var i = 0; i < len; i++){
      // console.log(GLOBAL_ALL.notifikasi[i].idNotifikasi +  ' == ' + idN)
      if(GLOBAL_ALL.notifikasi[i].idNotifikasi == idN){
        GLOBAL_ALL.notifikasi[i].read = true;
        break;
      }
    }; 
  })

  socket.on('like', function(dataLike){

    // Mencari index status tertentu pada list berdasarkan idStatus
    // Mengembalikan indeks status jika ada, jika tidak ada -1
    var findIndex = function(idStatus){
      var dataStatusX, indeksLike;

      if(pengelolaHalaman.halamanSaatIni().alamat == 'timeline')
        dataStatusX = GLOBAL_ALL.statusTimeline

      if(pengelolaHalaman.halamanSaatIni().alamat == 'my_profile')
        dataStatusX = GLOBAL_MY_PROFILE.dataStatus

      if(pengelolaHalaman.halamanSaatIni().alamat == 'profile')
        dataStatusX = GLOBAL_PROFILE.dataStatus

      for(var i = 0; i < dataStatusX.length; i++)
        if(dataStatusX[i].idStatus == idStatus)
          return i;

      return -1;
    }

    // Jika tidak berada pada halaman status, maka indeks dari
    // status yang di like harus dicari terlebih dahulu, lalu
    // data diubah otomatis binding ke view
    if(pengelolaHalaman.halamanSaatIni().alamat != 'status'){
      indeksLike = findIndex(dataLike.idStatus);

      // Jika pada timeline
      if(pengelolaHalaman.halamanSaatIni().alamat == 'timeline'){
        GLOBAL_ALL.statusTimeline[indeksLike].nLike += dataLike.like == 0 ? -1 : 1;
        GLOBAL_ALL.statusTimeline[indeksLike].like = dataLike.like;
      }

      // Jika pada my_profile
      if(pengelolaHalaman.halamanSaatIni().alamat == 'my_profile'){
        GLOBAL_MY_PROFILE.dataStatus[indeksLike].nLike += dataLike.like == 0 ? -1 : 1;
        GLOBAL_MY_PROFILE.dataStatus[indeksLike].like = dataLike.like;
      }

      // Jika pada profile
      if(pengelolaHalaman.halamanSaatIni().alamat == 'profile'){
        GLOBAL_PROFILE.dataStatus[indeksLike].nLike += dataLike.like == 0 ? -1 : 1;
        GLOBAL_PROFILE.dataStatus[indeksLike].like = dataLike.like;
      }
    }

    // Perlakukan khusus jika sedang berada pada halaman status,
    // cukup melakukan perubahan data pada paramPerpindahan['status']
    // yang menyimpan data-data tentang status tersebut
    if(pengelolaHalaman.halamanSaatIni().alamat == 'status'){
      pengelolaHalaman.paramPerpindahan['status'].nLike += dataLike.like == 0 ? -1 : 1;
      pengelolaHalaman.paramPerpindahan['status'].like = dataLike.like;
    }

    // Apply agar like terlihat
    scopePtr[pengelolaHalaman.halamanSaatIni().alamat].$apply();

  });

  socket.on('komentar', function(data){

    // Komentar hanya dapat dilakukan pada halaman status

    // Komentar diterima server dengan baik
    if(data == '1'){

      // Mencari index status ... ?
      var findIndex = function(idStatus){
        for(var i = 0; i < GLOBAL_ALL.statusTimeline.length; i++)  
          if(GLOBAL_ALL.statusTimeline[i].idStatus == idStatus)
            return i;

        return -1;
      }

      var indeksKomentar = findIndex(pengelolaHalaman.paramPerpindahan['status'].idStatus);
      GLOBAL_ALL.statusTimeline[indeksKomentar].komentar.unshift({
        nama: GLOBAL_PROFILE.nama,
        isiKomentar: scopePtr[pengelolaHalaman.halamanSaatIni().alamat].in_komentar
      });
      scopePtr[pengelolaHalaman.halamanSaatIni().alamat].dataKomentar.push({
        nama: GLOBAL_PROFILE.nama,
        isiKomentar: scopePtr[pengelolaHalaman.halamanSaatIni().alamat].in_komentar
      });
      GLOBAL_ALL.statusTimeline[indeksKomentar].nKomentar++;
      scopePtr[pengelolaHalaman.halamanSaatIni().alamat].in_komentar = '';
      
      scopePtr[pengelolaHalaman.halamanSaatIni().alamat].$apply();
    }
  });
// --- closeup ---


  document.addEventListener("backbutton", onBackButton, false);
}

function onBackButton(){
  pengelolaHalaman.mundur();
}

var regex = {
  phoneNo: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  name: /^[\p{L} \.'\-]+$/
}

function regexValid(regexData, data){
  return regexData.test(data);
}
























// --- Fuck up ---

  var GLOBAL_username = undefined;
  var GLOBAL_PROFILE = {};
  var GLOBAL_MY_PROFILE = {};
  var GLOBAL_ALL = {
    statusTimeline: [],
    pesan: [],
    notifikasi: []
  }

  var scopePtr = {};






















var app = angular.module('jengDixi', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
  .when('/home', {
        templateUrl: 'templates/home.html',
        controller: 'HomeController'
    }).when('/login', {
        templateUrl: 'templates/login.html',
        controller: 'LoginController'
    }).when('/verifikasi', {
        templateUrl: 'templates/verifikasi.html',
        controller: 'VerifikasiController'
    }).when('/register2', {
        templateUrl: 'templates/register2.html',
        controller: 'Register2Controller'
    }).when('/notification', {
        templateUrl: 'templates/notification.html',
        controller: 'NotificationController'
    }).when('/shoppink', {
        templateUrl: 'templates/shoping.html',
        controller: 'ShoppinkController'
    }).when('/forum', {
        templateUrl: 'templates/forum.html',
        controller: 'ForumController'
    }).when('/delivery', {
        templateUrl: 'templates/delivery.html',
        controller: 'DeliveryController'
    }).when('/timeline', {
        templateUrl: 'templates/timeline.html',
        controller: 'TimelineController',
        reload: true //will reload controller when state is being access
    }).when('/my_profile', {
        templateUrl: 'templates/my_profile.html',
        controller: 'MyProfileController'
    }).when('/status', {
        templateUrl: 'templates/status.html',
        controller: 'StatusController'
    }).when('/profile', {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileController'
    }).when('/chicchat', {
        templateUrl: 'templates/chicchat.html',
        controller: 'ChicchatController',
        reload: true //will reload controller when state is being access
    }).when('/private_chat', {
        templateUrl: 'templates/private_chat.html',
        controller: 'PrivateChatController',
        reload: true //will reload controller when state is being access
    }).when('/group_chat', {
        templateUrl: 'templates/group_chat.html',
        controller: 'GroupChatController',
        reload: true //will reload controller when state is being access
    }).when('/make_a_post', {
        templateUrl: 'templates/make_a_post.html',
        controller: 'MakeAPostController'
    }).
    otherwise({
      redirectTo: '/home'
    });
})

.factory('socket', function ($rootScope) {
  // console.log(socket);
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
})
















.controller('IndexCtrl', function($scope, $location){
  scopePtr['index'] = $scope;

  pengelolaHalaman.rootScope = $scope;
  pengelolaHalaman.locationScope = $location;
  $scope.kembali = function(){
    pengelolaHalaman.mundur();
  }
  $scope.keTimeline = function(){
    pengelolaHalaman.ke($location, 'timeline', undefined);
  }
  $scope.keChicchat = function(){
    pengelolaHalaman.ke($location, 'chicchat', undefined);
  }
  $scope.keNotifikasi = function(){
    pengelolaHalaman.ke($location, 'notification', undefined);
  }
})
















































// ______
//
//  HOME
// ______
//
.controller('HomeController', function ($scope, $location, $rootScope, socket){
  scopePtr['home'] = $scope;
  $rootScope.class_header = 'header-hide';
  $rootScope.class_header_chat = 'header-hide';
  $scope.popupRegister = function(){
    $scope.register_class_text_signup = 'register_signup_text_show';
    $scope.register_class_popup_registrasi = 'register_popupRegistrasi_show';
  }

  // $scope.hidePopupRegister = function(){
  //   $scope.register_class_text_signup = '';
  //   $scope.register_class_popup_registrasi = '';
  // }

  $scope.keLogin = function(){
    pengelolaHalaman.ke($location, 'login', undefined);
  }

  $scope.cekDataSignUp = function(){
    if($scope.in_email != undefined && $scope.in_password != undefined && $scope.in_password2 != undefined && $scope.in_noHP != undefined){
      var valid = regexValid(regex.email, $scope.in_email) && 
                  $scope.in_password.length > 0 &&
                  ($scope.in_password == $scope.in_password2);
      $scope.tombolOk = valid ? 'OK' : '';
      $scope.register_class_ok = 'register_kedaftar_show';
    }
  }

  $scope.daftarTahap1 = function(){
    pengelolaHalaman.paramPerpindahan['register1'] = {
      email: $scope.in_email,
      password: $scope.in_password,
      nohp: $scope.in_noHP
    };

    socket.emit('register1', {
      email: $scope.in_email,
      password: $scope.in_password,
      nohp: $scope.in_noHP
    });
  }

  $scope.pindahKeVerifikasi = function() {
    pengelolaHalaman.ke($location, 'verifikasi', undefined);
  }
})













































// _______
//
//  LOGIN
// _______
//
.controller('LoginController', function ($scope, $location, socket){
  scopePtr['login'] = $scope;

  socket.on('login', function(username){
    // console.log(username);
    if(username.length > 1){
      // console.log(username);
      GLOBAL_username = username;
      pengelolaHalaman.ke($location, 'timeline', undefined);
    }
  })

  // ng-click
  $scope.kembali = function(){
    pengelolaHalaman.mundur();
  }

  // ng-change
  $scope.loginCek = function(){
    if($scope.email != undefined && $scope.password != undefined)
      if(regexValid(regex.email, $scope.email) && $scope.password.length > 0 )
        $scope.class_login = 'login_tblLogin_show'
      else
        $scope.class_login = ''
  }

  // ng-click
  $scope.login = function(){
    console.log();
    socket.emit('login', {
      email: $scope.email,
      password: $scope.password
    });
  }
})













































// ____________
//
//  VERIFIKASI
// ____________
//
.controller('VerifikasiController', function ($scope, $location, $timeout){
  scopePtr['verifikasi'] = $scope;
  $timeout(function(){
    $location.path('register2');
  }, 3000);
})















































// ___________
//
//  REGISTER2
// ___________
//
.controller('Register2Controller', function ($scope, $location, socket){
  scopePtr['register2'] = $scope;
  var showOK = function(){
    $scope.tombolSignIn = 'OK';
  }

  var hideOK = function(){
    $scope.tombolSignIn = '';
  }

  // ng-src
  $scope.gbrCowo = 'img/male.png';

  // ng-src
  $scope.gbrCewe = 'img/female.png';

  // binding variable
  $scope.warning = 'Nama depan wajib diisi';

  // binding variable
  $scope.warningJK = 'Jenis kelamin wajib dipilih';

  // ng-change
  $scope.cekDataReg2 = function(){
    if($scope.in_namaDepan.length == 0){
      $scope.warning = 'Nama depan wajib diisi';
      hideOK();
    }else{
      $scope.warning = '';
      if($scope.warningJK == '')
        showOK();
    }
  }

  // ng-click
  $scope.pilihCowo = function(){
    $scope.warningJK = '';
    $scope.gbrCowo = 'img/male_select.png';
    $scope.gbrCewe = 'img/female.png';
    if($scope.warning == '')
      showOK();
  }

  // ng-click
  $scope.pilihCewe = function(){
    $scope.warningJK = '';
    $scope.gbrCowo = 'img/male.png';
    $scope.gbrCewe = 'img/female_select.png';
    if($scope.warning == '')
      showOK();
  }

  // ng-click
  $scope.daftarTahap2 = function(){
    socket.emit('register2', {
      email: pengelolaHalaman.paramPerpindahan['register1'].email,
      password: pengelolaHalaman.paramPerpindahan['register1'].password,
      nohp: pengelolaHalaman.paramPerpindahan['register1'].nohp,
      fisrtName: $scope.in_namaDepan,
      lastName: $scope.in_namaBelakang,
      jenisKelamin: $scope.gbrCowo == 'img/male_select.png' ? 1 : 2
    });
    console.log('harap tunggu...')
  }

  $scope.pindahKeTimeline = function() {
    pengelolaHalaman.ke($location, 'timeline', undefined);
  }
})












































// ______________
//
//  NOTIFICATION
// ______________
//
.controller('NotificationController', function ($scope, $location){
  scopePtr['notification'] = $scope;

  $scope.kembali = function(){
    pengelolaHalaman.kembali();
  }

  $scope.getJenisNotifikasi = function(x){
    switch(x){
      case 0: return 'comment on your';
      case 1: return 'has kiss on your';
      case 2: return 'follows you';
      case 3: return 'tag you at';
    }
  }

  $scope.getJenisSubNotifikasi = function(x){
    switch(x){
      case 0: return 'status';
      case 1: return 'photo';
      case 2: return '<location name>';
    }
  }

  $scope.keStatus = function(idStatus, idNotifikasi){
    socket.emit('bacaNotifikasi', {
      idNotifikasi: idNotifikasi
    });
    pengelolaHalaman.paramPerpindahan['status'] = {};
    pengelolaHalaman.paramPerpindahan['notification'] = idNotifikasi;
    pengelolaHalaman.paramPerpindahan['status'].idStatus = idStatus;
    pengelolaHalaman.ke($location, 'status', undefined);
  }

  $scope.dataNotifikasi = GLOBAL_ALL.notifikasi;
})















































// __________
//
//  SHOPPINK
// __________
//
.controller('ShoppinkController', function ($scope){
  scopePtr['shoppink'] = $scope;
})















































// __________
//
//  TIMELINE
// __________
//
.controller('TimelineController', function ($scope, $location, $rootScope, $timeout, $window, socket){
  scopePtr['timeline'] = $scope;

  // --- binding function --- //

  $scope.getDate = function (datetime){
    var d = new Date(datetime);
    return d.getDate() + '/' + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + '/' + d.getFullYear();
  }

  $scope.getTime = function (datetime){
    var d = new Date(datetime);
    return d.getHours() + ':' + d.getMinutes();
  }

  $scope.getIconLike = function(b){
    return b == '1' ? 'img/lips_full.png' : 'img/lips.png';
  }

  $scope.adaGambar = function(strGbr){
    return strGbr.substr(strGbr.length - 9, 9) != 'undefined';
  }

  // --- --- --- --- --- --- //

  GLOBAL_PROFILE = {
    gambar: 'img/fb.png',
    nama: 'harap tunggu'
  }

  GLOBAL_ALL.statusTimeline = [
    {
      UrlProfilPic:"img/fb.png",
      gambar:"",
      idStatus:17,
      isiStatus:"Sedang memuat, harap tunggu...",
      komentar:[],
      like:1,
      nKomentar:0,
      nLike:1,
      nama:"Harap tunggu...",
      tanggal:"2016-08-01T17:15:15.267Z",
      username:"tes"
    }
  ]

  $scope.user = GLOBAL_PROFILE;
  $scope.dataTimeline = GLOBAL_ALL.statusTimeline;

  $scope.olahDataInitAwal = function(data){
    
    if(GLOBAL_ALL.statusTimeline.length > 0)
      GLOBAL_ALL.statusTimeline = []

    if(GLOBAL_ALL.notifikasi.length > 0)
      GLOBAL_ALL.notifikasi = []

    if(GLOBAL_ALL.pesan.length > 0)
      GLOBAL_ALL.pesan = []

    data.dataStatus.forEach(function(status){
      GLOBAL_ALL.statusTimeline.push(status);
    });

    data.dataPesan.forEach(function(pesan){
      GLOBAL_ALL.pesan.push(pesan);
    });

    data.dataNotifikasi.forEach(function(notif){
      GLOBAL_ALL.notifikasi.push(notif);
    });

    GLOBAL_PROFILE['gambar'] = data.dataProfilPribadi['gambar'];
    GLOBAL_PROFILE['nama'] = data.dataProfilPribadi['nama'];
    // alert(GLOBAL_PROFILE.nama);

    $scope.dataTimeline = GLOBAL_ALL.statusTimeline;
    $scope.$apply();
  }

  // 
  socket.emit('initAwal', {
    username: GLOBAL_username,
    idPesanTerakhir: 0,
    idNotifikasiTerakhir: 0
  });

  // -------------------------------------------
  // Mengontrol FAB agar hide/show ketika scroll
  // -------------------------------------------
  var lastScrollTop = 0;
  var direction = "";
  angular.element($window).bind("scroll", function() {
    var st = window.pageYOffset;
    if (st > lastScrollTop) {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = 'fab-bag-pull-hide';
    } else {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = '';
    }

    $scope.$apply();

    lastScrollTop = st;
  });
  // -------------------------------------------

  // ng-click
  $scope.showFAB = function(){
    $scope.class_fab_post = 'fab-post-trans';
    $scope.class_fab_delivery = 'fab-delivery-trans';
    $scope.class_fab_shopink = 'fab-shopink-trans';
    $scope.class_fab_forum = 'fab-forum-trans';
    $scope.class_fab_close = 'fab-close-trans';
    $scope.class_fab_bag = 'fab-bag-hide';

    $timeout(function(){
      $scope.class_label_post = 'fab-label-show';
      $scope.class_label_delivery = 'fab-label-show';
      $scope.class_label_shopink = 'fab-label-show';
      $scope.class_label_forum = 'fab-label-show';
    }, 250);
  }

  // ng-click
  $scope.hideFAB = function(){
    $scope.class_label_post = '';
    $scope.class_label_delivery = '';
    $scope.class_label_shopink = '';
    $scope.class_label_forum = '';

    $scope.class_fab_post = '';
    $scope.class_fab_delivery = '';
    $scope.class_fab_shopink = '';
    $scope.class_fab_forum = '';
    $scope.class_fab_close = '';
    $scope.class_fab_bag = '';
  }

  // ng-click
  $scope.like = function(idStatus){
    socket.emit('like', {
      username: GLOBAL_username,
      idStatus: idStatus
    });
  }

  // ng-click
  $scope.keStatus = function(status){
    pengelolaHalaman.paramPerpindahan['status'] = status;
    pengelolaHalaman.ke($location, 'status', undefined);
  }

  // ng-click
  $scope.keNotifikasi = function(){
    pengelolaHalaman.ke($location, 'notification', undefined);
  }

  // ng-click
  $scope.keChicChat = function(){
    pengelolaHalaman.ke($location, 'chicchat', undefined);
  }

  // ng-click
  $scope.keShoppink = function(){
    pengelolaHalaman.ke($location, 'shoppink', undefined);
  }

  // ng-click
  $scope.keForum = function(){
    pengelolaHalaman.ke($location, 'forum', undefined);
  }

  // ng-click
  $scope.keDelivery = function(){
    pengelolaHalaman.ke($location, 'delivery', undefined);
  }

  // ng-click
  $scope.keMakeAPost = function(){
    pengelolaHalaman.ke($location, 'make_a_post', undefined);
  }

  // ng-click
  $scope.keMyProfile = function(){
    pengelolaHalaman.ke($location, 'my_profile', undefined);
  }

  // ng-click
  $scope.keProfile = function(nama, username){
    if(username == GLOBAL_username){
      pengelolaHalaman.ke($location, 'my_profile', undefined);
      return;
    }
    pengelolaHalaman.paramPerpindahan['profile'] = username;
    pengelolaHalaman.ke($location, 'profile', {
      requestJudulHalaman: nama,
      requestJumlah: undefined
    });
  }
})













































// ____________
//
//  MY PROFILE
// ____________
//
.controller('MyProfileController', function ($scope, $location, $rootScope, $timeout, $window, socket){
  scopePtr['my_profile'] = $scope;


  // --- binding function --- //

  $scope.getDate = function (datetime){
    var d = new Date(datetime);
    return d.getDate() + '/' + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + '/' + d.getFullYear();
  }

  $scope.getTime = function (datetime){
    var d = new Date(datetime);
    return d.getHours() + ':' + d.getMinutes();
  }

  $scope.getIconLike = function(b){
    return b == '1' ? 'img/lips_full.png' : 'img/lips.png';
  }

  // --- --- --- --- --- --- //

  socket.emit('ambilStatusProfil', {
    username: GLOBAL_username
  })

  socket.on('ambilStatusProfil', function(data){
    // console.log(data);
    GLOBAL_MY_PROFILE = data;

    $scope.myProfile = GLOBAL_MY_PROFILE.dataProfile;
    $scope.dataTimeline = GLOBAL_MY_PROFILE.dataStatus;
  })

  // -------------------------------------------
  // Mengontrol FAB agar hide/show ketika scroll
  // -------------------------------------------
  var lastScrollTop = 0;
  var direction = "";
  angular.element($window).bind("scroll", function() {
    var st = window.pageYOffset;
    if (st > lastScrollTop) {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = 'fab-bag-pull-hide';
    } else {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = '';
    }

    $scope.$apply();

    lastScrollTop = st;
  });
  // -------------------------------------------

  // ng-click
  $scope.showFAB = function(){
    $scope.class_fab_post = 'fab-post-trans';
    $scope.class_fab_delivery = 'fab-delivery-trans';
    $scope.class_fab_shopink = 'fab-shopink-trans';
    $scope.class_fab_forum = 'fab-forum-trans';
    $scope.class_fab_close = 'fab-close-trans';
    $scope.class_fab_bag = 'fab-bag-hide';

    $timeout(function(){
      $scope.class_label_post = 'fab-label-show';
      $scope.class_label_delivery = 'fab-label-show';
      $scope.class_label_shopink = 'fab-label-show';
      $scope.class_label_forum = 'fab-label-show';
    }, 250);
  }

  // ng-click
  $scope.hideFAB = function(){
    $scope.class_label_post = '';
    $scope.class_label_delivery = '';
    $scope.class_label_shopink = '';
    $scope.class_label_forum = '';

    $scope.class_fab_post = '';
    $scope.class_fab_delivery = '';
    $scope.class_fab_shopink = '';
    $scope.class_fab_forum = '';
    $scope.class_fab_close = '';
    $scope.class_fab_bag = '';
  }

  // ng-click
  $scope.like = function(idStatus){
    socket.emit('like', {
      username: GLOBAL_username,
      idStatus: idStatus
    });
  }

  // ng-click
  $scope.keStatus = function(status){
    pengelolaHalaman.paramPerpindahan['status'] = status;
    pengelolaHalaman.ke($location, 'status', undefined);
  }
  
  // ng-click
  $scope.keNotifikasi = function(){
    pengelolaHalaman.ke($location, 'notification', undefined);
  }

  // ng-click
  $scope.keChicChat = function(){
    pengelolaHalaman.ke($location, 'chicchat', undefined);
  }

  // ng-click
  $scope.keShoppink = function(){
    pengelolaHalaman.ke($location, 'shoppink', undefined);
  }

  // ng-click
  $scope.keForum = function(){
    pengelolaHalaman.ke($location, 'forum', undefined);
  }

  // ng-click
  $scope.keDelivery = function(){
    pengelolaHalaman.ke($location, 'delivery', undefined);
  }

  // ng-click
  $scope.keMakeAPost = function(){
    pengelolaHalaman.ke($location, 'make_a_post', undefined);
  }  
})













































// _________
//
//  PROFILE
// _________
//
.controller('ProfileController', function ($scope, $location, $rootScope, $timeout, $window, socket){
  scopePtr['profile'] = $scope;


  // --- binding function --- //

  $scope.getDate = function (datetime){
    var d = new Date(datetime);
    return d.getDate() + '/' + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + '/' + d.getFullYear();
  }

  $scope.getTime = function (datetime){
    var d = new Date(datetime);
    return d.getHours() + ':' + d.getMinutes();
  }

  $scope.getIconLike = function(b){
    // console.log("like", b)
    return b == '1' ? 'img/lips_full.png' : 'img/lips.png';
  }

  // --- --- --- --- --- --- //

  socket.emit('ambilStatusProfil', {
    username: pengelolaHalaman.paramPerpindahan['profile']
  })

  socket.on('ambilStatusProfil', function(data){
    // console.log(data);
    GLOBAL_PROFILE = data;

    $scope.profile = GLOBAL_PROFILE.dataProfile;
    $scope.dataTimeline = GLOBAL_PROFILE.dataStatus;
  })

  // -------------------------------------------
  // Mengontrol FAB agar hide/show ketika scroll
  // -------------------------------------------
  var lastScrollTop = 0;
  var direction = "";
  angular.element($window).bind("scroll", function() {
    var st = window.pageYOffset;
    if (st > lastScrollTop) {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = 'fab-bag-pull-hide';
    } else {
        if($scope.class_fab_bag != 'fab-bag-hide')
          $scope.class_fab_bag = '';
    }

    $scope.$apply();

    lastScrollTop = st;
  });
  // -------------------------------------------

  // ng-click
  $scope.showFAB = function(){
    $scope.class_fab_post = 'fab-post-trans';
    $scope.class_fab_delivery = 'fab-delivery-trans';
    $scope.class_fab_shopink = 'fab-shopink-trans';
    $scope.class_fab_forum = 'fab-forum-trans';
    $scope.class_fab_close = 'fab-close-trans';
    $scope.class_fab_bag = 'fab-bag-hide';

    $timeout(function(){
      $scope.class_label_post = 'fab-label-show';
      $scope.class_label_delivery = 'fab-label-show';
      $scope.class_label_shopink = 'fab-label-show';
      $scope.class_label_forum = 'fab-label-show';
    }, 250);
  }

  // ng-click
  $scope.hideFAB = function(){
    $scope.class_label_post = '';
    $scope.class_label_delivery = '';
    $scope.class_label_shopink = '';
    $scope.class_label_forum = '';

    $scope.class_fab_post = '';
    $scope.class_fab_delivery = '';
    $scope.class_fab_shopink = '';
    $scope.class_fab_forum = '';
    $scope.class_fab_close = '';
    $scope.class_fab_bag = '';
  }

  // ng-click
  $scope.like = function(idStatus){
    socket.emit('like', {
      username: GLOBAL_username,
      idStatus: idStatus
    });
  }

  // ng-click
  $scope.keStatus = function(status){
    pengelolaHalaman.paramPerpindahan['status'] = status;
    pengelolaHalaman.ke($location, 'status', undefined);
  }

  // ng-click
  $scope.keNotifikasi = function(){
    pengelolaHalaman.ke($location, 'notification', undefined);
  }

  // ng-click
  $scope.keChicChat = function(){
    pengelolaHalaman.ke($location, 'chicchat', undefined);
  }

  // ng-click
  $scope.keShoppink = function(){
    pengelolaHalaman.ke($location, 'shoppink', undefined);
  }

  // ng-click
  $scope.keForum = function(){
    pengelolaHalaman.ke($location, 'forum', undefined);
  }

  // ng-click
  $scope.keDelivery = function(){
    pengelolaHalaman.ke($location, 'delivery', undefined);
  }

  // ng-click
  $scope.keMakeAPost = function(){
    pengelolaHalaman.ke($location, 'make_a_post', undefined);
  }
})













































// __________
//
//  CHICCHAT
// __________
//
.controller('ChicchatController', function ($scope, $location, $rootScope){
  scopePtr['chicchat'] = $scope;
  $scope.keTimeline = function(){
    pengelolaHalaman.ke($location, 'timeline', undefined);
  }
  $scope.img_group = 'img/down.png';
  $scope.img_group_invitation = 'img/down.png';
  $scope.img_friend = 'img/down.png';
  $scope.class_list_factory_group = 'list_factory_hide';
  $scope.class_list_factory_group_invitation = 'list_factory_hide';
  $scope.class_list_factory_friend = 'list_factory_hide';

  $scope.dataChat = {
    group: {
      members: []
    },
    groupInvitation: {
      members: []
    },
    friend: {
      members: []
    }
  }

  $scope.showGroup = function(){
    if($scope.img_group == 'img/down.png'){
      $scope.class_list_factory_group = 'list_factory_show';
      $scope.img_group = 'img/up.png';
      $scope.class_judul_chat_group = 'judul-show';
    }else{
      $scope.class_list_factory_group = 'list_factory_hide';
      $scope.img_group = 'img/down.png';
      $scope.class_judul_chat_group = '';
    }
  };

  $scope.showGroupInvitation = function(){
    if($scope.img_group_invitation == 'img/down.png'){
      $scope.class_list_factory_group_invitation = 'list_factory_show';
      $scope.img_group_invitation = 'img/up.png';
      $scope.class_judul_chat_group_invitation = 'judul-show';
    }else{
      $scope.class_list_factory_group_invitation = 'list_factory_hide';
      $scope.img_group_invitation = 'img/down.png';
      $scope.class_judul_chat_group_invitation = '';
    }
  };

  $scope.showFriend = function(){
    if($scope.img_friend == 'img/down.png'){
      $scope.class_list_factory_friend = 'list_factory_show';
      $scope.img_friend = 'img/up.png';
      $scope.class_judul_chat_friend = 'judul-show';
    }else{
      $scope.class_list_factory_friend = 'list_factory_hide';
      $scope.img_friend = 'img/down.png';
      $scope.class_judul_chat_friend = '';
    }
  };

  var dataPesan = [];

  // console.log("xxx", GLOBAL_ALL.pesan)

  // mengecek apakah user sudah ada pada data pesan
  function isPesanAda(username){
    var lenPesan = dataPesan.length
    var usernameCheckKey = username == GLOBAL_username ? 'usernamePengirim' : 'usernamePenerima';
    // console.log('cek isPesanAda');
    for(var i = 0; i < lenPesan; i++){
      if(dataPesan[i][usernameCheckKey] == username){
        // console.log('cek -> ' + dataPesan[i][usernameCheckKey] + ' : ' + username);
        return i;
      }
    }

    return -1;
  }

  var hitungPesanBelumDibaca = function(username){
    var lenPesan = GLOBAL_ALL.pesan.length;
    var counter = 0;

    // iterasi pesan
    for(var i = 0; i < lenPesan; i++)
      if(GLOBAL_ALL.pesan[i].usernamePengirim == username && GLOBAL_ALL.pesan[i].read == false)
        counter++;

    return counter == 0 ? '' : counter;
  }

  // Latar belakang: ketika pengguna masuk ke halaman chicchat,
  // data pesan yang terdapat pada chicchat harus diupdate sesuai dengan
  // data terbaru pesan (yang terakhir kali diambil dari server)
  // 
  // Setiap pesan yang terdapat pada larik GLOBAL_ALL['pesan'] diiterasi
  // dan setiap pesan dari pengirim atau penerima yang unik dijadikan sebuah 
  // list pesan baru pada chicchat.
  $scope.updatePesan = function(){
    var lenPesan = GLOBAL_ALL.pesan.length;

    var waktuUpdateTerakhir = new Date();

    // iterasi pesan
    for(var i = 0; i < lenPesan; i++){

      // mengambil username pengirim/penerima tergantung situasi pesan
      var usernameCheck = GLOBAL_ALL.pesan[i].usernamePenerima == GLOBAL_username
                          ? GLOBAL_ALL.pesan[i].usernamePengirim
                          : GLOBAL_ALL.pesan[i].usernamePenerima;
      // console.log(usernameCheck + ' - ' + GLOBAL_username);

      // jika pesan dengan user yang unik (blm ada pada list chat)
      // maka ditambahkan
      var ipa = isPesanAda(usernameCheck);
      console.log('hasil => ' + ipa);
      if(ipa == -1){

        // request socket untuk mengambil gambar dan nama lawan pesan
        socket.emit('ambilNamaGambar', {
          username: usernameCheck
        })

        // apply data ke list yang akan ditampilkan
        var d = new Date(GLOBAL_ALL.pesan[i].tanggal);
        var tgl = d.getHours() + ':' + d.getMinutes();
        var nBlmDibaca = hitungPesanBelumDibaca(usernameCheck);
        dataPesan.push({
          nama: GLOBAL_ALL.pesan[i].in_nama,
          gambar: GLOBAL_ALL.pesan[i].in_gambar,
          usernamePenerima: usernameCheck,
          isiPesan: GLOBAL_ALL.pesan[i].isiPesan,
          tanggal: tgl,
          nPesanBelumDibaca: nBlmDibaca,
          updateTerakhir: waktuUpdateTerakhir
        })
      }else{
        if(dataPesan[ipa].updateTerakhir != waktuUpdateTerakhir){

          var d = new Date(GLOBAL_ALL.pesan[i].tanggal);
          var tgl = d.getHours() + ':' + d.getMinutes();
          dataPesan[ipa].tanggal = tgl;
          dataPesan[ipa].nPesanBelumDibaca = hitungPesanBelumDibaca(usernameCheck);
          dataPesan[ipa].updateTerakhir = waktuUpdateTerakhir;
          dataPesan[ipa].isiPesan = GLOBAL_ALL.pesan[i].isiPesan
        }
      }
    }

    // mengirim data pesan yang sudah dikelompokkan menjadi list chat
    // ke view halaman
    $scope.dataPesan = dataPesan;
  }

  // update pesan, setiap kali masuk halaman chicchat
  $scope.updatePesan();

  $scope.ambilNamaUser = function(s){
    return s;
  }

  $scope.ambilSummaryPesan = function(s){
    if(s.length < 20)
      return s;

    return s.substr(0, 17) + '...';
  }

  $scope.ambilWaktuPesan = function(tgl){
    return tgl;
    // console.log('@');
    // console.log('@');
    // console.log('@');
    // console.log(tgl);
    // console.log('@');
    // console.log('@');
    // console.log('@');

    // if(tgl == -1)
    //   return 'sending';

    // var d = new Date(tgl);
    // return d.getHours() + ':' + d.getMinutes();
  }

  $scope.keFriendChat = function(member){
    pengelolaHalaman.paramPerpindahan['private_chat'] = {usernamePenerima: msg.usernamePenerima, gambar: msg.gambar};
    pengelolaHalaman.ke($location, 'private_chat', {
      requestJudulHalaman: member.nama,
      requestJumlah: undefined
    });
  }

  $scope.keGroupChat = function(member){
    pengelolaHalaman.ke($location, 'group_chat', {
      requestJudulHalaman: member.nama,
      requestJumlah: member.jumlah
    });
  }

  $scope.bukaPesan = function(msg){
    pengelolaHalaman.paramPerpindahan['private_chat'] = {usernamePenerima: msg.usernamePenerima, gambar: msg.gambar};
    pengelolaHalaman.ke($location, 'private_chat', {
      requestJudulHalaman: msg.nama,
      requestJumlah: undefined
    });
  }
})















































// ______________
//
//  PRIVATE CHAT
// ______________
//
.controller('PrivateChatController', function ($scope, $rootScope, socket, $anchorScroll, $location, $timeout){
  scopePtr['private_chat'] = $scope;
  $rootScope.toggleOptionChat = function(){
    if($scope.class_bar_opsi_chat == 'chatPrivate_bar_opsi-trans-hide')
      $scope.class_bar_opsi_chat = '';
    else
      $scope.class_bar_opsi_chat = 'chatPrivate_bar_opsi-trans-hide';
  }

  $scope.gotoBottom = function(){
    // scroll to bottom
    $location.hash('bottomPC');
    $anchorScroll();
  }

  $scope.kirimPesan = function(pesan){
    socket.emit('kirimPrivate', {
      usernamePengirim: GLOBAL_username,
      usernamePenerima: pengelolaHalaman.paramPerpindahan['private_chat'].usernamePenerima,
      isiPesan: pesan
    })

    pesanTerkirim = {
      idPesan: undefined,
      isiPesan: pesan,
      read: false,
      tanggal: -1,
      usernamePengirim: GLOBAL_username,
      usernamePenerima: pengelolaHalaman.paramPerpindahan['private_chat'].usernamePenerima
    };

    GLOBAL_ALL.pesan.unshift(pesanTerkirim)

    $scope.dataPesan.push(pesanTerkirim)

    $scope._pesan_ = '';
    $timeout(function() {
      $scope.gotoBottom();
    }, 100);

    document.getElementById('inputFokus').focus();
  }

  $scope.ambilGambar = function(){
    return pengelolaHalaman.paramPerpindahan['private_chat'].gambar
  }

  $scope.pesanKiri = function(username){
    return username != GLOBAL_username;
  }

  $scope.ambilWaktuPesan = function(tgl){
    if(tgl == -1)
      return 'sending';

    var d = new Date(tgl);
    return d.getHours() + ':' + d.getMinutes();
  }

  var dataPesanPrivate = [];

  var lenPP = GLOBAL_ALL.pesan.length;
  for(var i = lenPP - 1; i > -1; i--){
    var keyUsernameChat = GLOBAL_ALL.pesan[i].usernamePenerima == GLOBAL_username ? 'usernamePengirim' : 'usernamePenerima';

    if(GLOBAL_ALL.pesan[i][keyUsernameChat] == pengelolaHalaman.paramPerpindahan['private_chat'].usernamePenerima){
      if(GLOBAL_ALL.pesan[i].usernamePenerima == GLOBAL_username && !GLOBAL_ALL.pesan[i].read){
        GLOBAL_ALL.pesan[i].read = true;
        socket.emit('pesanDibaca', {
          idPesan: GLOBAL_ALL.pesan[i].idPesan,
          usernamePengirim: GLOBAL_ALL.pesan[i].usernamePengirim
        });
      }
      dataPesanPrivate.push(GLOBAL_ALL.pesan[i]);
    }
  }

  // console.log(dataPesanPrivate)

  $scope.dataPesan = dataPesanPrivate;

  $timeout(function() {
    $scope.gotoBottom();
  }, 500);
})
















































// ____________
//
//  GROUP CHAT
// ____________
//
.controller('GroupChatController', function ($scope, $rootScope){
  scopePtr['group_chat'] = $scope;
  $rootScope.toggleOptionChat = function(){
    if($scope.class_bar_opsi_chat == 'chatPrivate_bar_opsi-trans-hide')
      $scope.class_bar_opsi_chat = '';
    else
      $scope.class_bar_opsi_chat = 'chatPrivate_bar_opsi-trans-hide';
  }
})













































// _____________
//
//  MAKE A POST
// _____________
//
.controller('MakeAPostController', function ($scope, $location, socket){
  scopePtr['make_a_post'] = $scope;

  socket.on('makeAPost', function(dataStatusTerkirim){
    // console.log(dataStatusTerkirim);
    GLOBAL_ALL.statusTimeline.unshift({
      UrlProfilPic: GLOBAL_PROFILE.gambar,
      idStatus: dataStatusTerkirim.idStatus,
      gambar: '',
      isiStatus: $scope.isiStatus,
      komentar: [],
      like: false,
      nKomentar: 0,
      nLike: 0,
      nama: GLOBAL_PROFILE.nama,
      tanggal: dataStatusTerkirim.tanggal,
      username: GLOBAL_username
    });
    $scope.isiStatus = '';
    pengelolaHalaman.ke($location, 'timeline', undefined);
  })

  $scope.postStatus = function(){
    console.log($scope.isiStatus);
    socket.emit('makeAPost', {
      username: GLOBAL_username,
      isiStatus: $scope.isiStatus
    })
  }
})













































// __________
//
//  DELIVERY
// __________
//
.controller('DeliveryController', function ($scope){
  scopePtr['delivery'] = $scope;
  
})














































// __________
//
//  STATUS
// __________
//
.controller('StatusController', function ($scope, socket){
  scopePtr['status'] = $scope;

  // --- binding function --- //


  $scope.getDate = function (datetime){
    var d = new Date(datetime);
    return d.getDate() + '/' + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + '/' + d.getFullYear();
  }

  $scope.getTime = function (datetime){
    var d = new Date(datetime);
    return d.getHours() + ':' + d.getMinutes();
  }

  $scope.getIconLike = function(b){
    return b == '1' ? 'img/lips_full.png' : 'img/lips.png';
  }

  // --- --- --- --- --- --- //

  // --- socket start --- //

  socket.on('ambilStatus', function(dataStatus){
    console.log(dataStatus[0]);
    pengelolaHalaman.paramPerpindahan['status'] = dataStatus[0];
    $scope.status = pengelolaHalaman.paramPerpindahan['status'];
    socket.emit('ambilKomentar', {
      idStatus: pengelolaHalaman.paramPerpindahan['status'].idStatus
    });

    // $scope.$apply();
  });

  socket.on('ambilKomentar', function(arrKomentar){
    $scope.dataKomentar = arrKomentar;
  });


  socket.emit('ambilStatus', {
    username: GLOBAL_username,
    idStatus: pengelolaHalaman.paramPerpindahan['status'].idStatus
  });

  // --- socket end --- //

  $scope.like = function(idStatus){
    socket.emit('like', {
      username: GLOBAL_username,
      idStatus: idStatus
    });
  }

  $scope.kirimKomentar = function(idStatus, isiKomentar){
    socket.emit('komentar', {
      username: GLOBAL_username,
      idStatus: idStatus,
      isiKomentar: $scope.in_komentar
    });
  }
})













































// _______
//
//  FORUM
// _______
//
.controller('ForumController', function ($scope){
  scopePtr['forum'] = $scope;
  $scope.dataForum = [
    {
      user: {
        nama: 'Food & Beauty',
        fotoProfil: 'img/gbr1.jpg'
      },
      jumlahAnggota: 160,
      isiforum_post: 'Hello World',
      gambar: 'img/5.jpg',
      nLike: 25,
      nKomentar: 12
    },
    {
      user: {
        nama: 'Food & Beauty',
        fotoProfil: 'img/gbr1.jpg'
      },
      jumlahAnggota: 160,
      isiforum_post: 'Hello World',
      gambar: 'img/5.jpg',
      nLike: 25,
      nKomentar: 12
    },
    {
      user: {
        nama: 'Food & Beauty',
        fotoProfil: 'img/gbr1.jpg'
      },
      jumlahAnggota: 160,
      isiforum_post: 'Hello World',
      gambar: 'img/5.jpg',
      nLike: 25,
      nKomentar: 12
    }
  ];
});